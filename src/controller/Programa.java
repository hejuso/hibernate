package controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.hibernate.Session;

import model.Empresa;
import model.Item;
import model.Pedido;

public class Programa {

	public static void main(String[] args) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		Session session = UtilidadesHibernate.getSessionFactory().openSession();
		session.beginTransaction();

		// INSERTAR DATOS
		Empresa e = new Empresa();
		Item i = new Item();
		Pedido p = new Pedido();

		// Insertamos empresa
		e.setCif("20844022M");
		e.setDireccion("C/ Falsa, 123");
		e.setEmpleados(6);
		e.setNombre("Inditex");
		session.save(e);

		// Insertamos pedido
		p.setId(2);
		p.setFecha(date);
		// Insertamos items al pedido
		p.getItems().add(new Item(1,"Esmoquin",2));
		p.getItems().add(new Item(1,"Esmoquin",2));
		p.getItems().add(new Item(1,"Esmoquin",2));
		session.save(p);


		/*
		 * session.getTransaction().commit();
		 * 
		 * session.beginTransaction();
		 * 
		 * Usuario u = session.get(Usuario.class, 1);
		 */

		// RECUPERAR DATOS
		/*
		 * Empresa e = session.get(Empresa.class, "20844022M"); Item i =
		 * session.get(Item.class, 1); Pedido p = session.get(Pedido.class, 1);
		 * 
		 * System.out.println("------------------");
		 * System.out.println("Nombre empresa: "+ e.getNombre());
		 * System.out.println("Nombre del item: "+ i.getNombre());
		 * System.out.println("Nombre empresa: "+ p.getFecha());
		 * System.out.println("------------------");
		 */
		session.getTransaction().commit();

		session.close();
		UtilidadesHibernate.getSessionFactory().close();
	}

}
