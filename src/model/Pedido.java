package model;

import java.util.ArrayList;
import java.util.List;

public class Pedido {

	int id;
	java.util.Date fecha;
	private List<Item> items = new ArrayList<Item>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public java.util.Date getFecha() {
		return fecha;
	}
	public void setFecha(java.util.Date date) {
		this.fecha = date;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
}
